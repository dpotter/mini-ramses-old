!#########################################################
!#########################################################
!#########################################################
!#########################################################
subroutine m_force_fine(pst,ilevel,icount)
  use amr_parameters, only: ndim,twotondim,nvector,dp
  use ramses_commons, only: pst_t
  implicit none
  type(pst_t)::pst
  integer::ilevel,icount
  !----------------------------------------------------------
  ! This routine computes the gravitational acceleration,
  ! the maximum density rho_max, and the potential energy
  !----------------------------------------------------------
  integer,dimension(1:2)::input_array,output_array
  real(kind=8)::rhomax,epot
 
  if(pst%s%m%noct_tot(ilevel)==0)return
  if(pst%s%r%verbose)write(*,'("   Entering force_fine for level ",I2)')ilevel

#ifdef GRAV  

  if(pst%s%r%gravity_type>0)then 
     ! Compute analytical gravity force
     call r_force_analytic(pst,1,0,ilevel)
  else
     ! Compute gradient of potential
     input_array(1)=ilevel
     input_array(2)=icount
     call r_gradient_phi(pst,2,0,input_array)
  endif
  if(pst%s%r%verbose)write(*,'("   Gradient phi done for level ",I2)')ilevel

  ! Compute gravity potential energy
  call r_compute_epot(pst,1,2,ilevel,output_array)
  epot=transfer(output_array,epot)
  pst%s%g%epot_tot=pst%s%g%epot_tot+epot
  if(pst%s%r%verbose)write(*,'("   Potential energy done for level ",I2)')ilevel

  ! Compute maximum mass density
  call r_compute_rhomax(pst,1,2,ilevel,output_array)
  rhomax=transfer(output_array,rhomax)
  pst%s%g%rho_max(ilevel)=rhomax
  if(pst%s%r%verbose)write(*,'("   Maximum density done for level ",I2)')ilevel

#endif  

end subroutine m_force_fine
!#########################################################
!#########################################################
!#########################################################
!#########################################################
recursive subroutine r_force_analytic(pst,input_size,output_size,ilevel)
  use ramses_commons, only: pst_t
  use mdl_parameters
  implicit none
  type(pst_t)::pst
  integer::input_size,output_size
  integer::ilevel

  if(pst%nLower>0)then
     call mdl_send_request(pst%s%mdl,MDL_FORCE_ANALYTIC,pst%iUpper+1,input_size,output_size,ilevel)
     call r_force_analytic(pst%pLower,input_size,output_size,ilevel)
     call mdl_get_reply(pst%s%mdl,pst%iUpper+1,output_size)
  else
     call force_analytic(pst%s%r,pst%s%g,pst%s%m,ilevel)
  endif

end subroutine r_force_analytic
!#########################################################
!#########################################################
!#########################################################
!#########################################################
subroutine force_analytic(r,g,m,ilevel)
  use amr_parameters, only: ndim,twotondim,nvector,dp
  use amr_commons, only: run_t,global_t,mesh_t
  implicit none
  type(run_t)::r
  type(global_t)::g
  type(mesh_t)::m
  integer::ilevel
  !-------------------------------------
  ! Compute analytical gravity force
  !-------------------------------------
  integer::igrid,ind,i,ngrid,idim,nstride
  real(dp)::dx
  real(dp),dimension(1:nvector,1:ndim),save::xx,ff
 
#ifdef GRAV  

  ! Mesh size at level ilevel in code units
  dx=r%boxlen/2**ilevel

  ! Loop over grids by vector sweeps
  do igrid=m%head(ilevel),m%tail(ilevel),nvector
     ngrid=MIN(nvector,m%tail(ilevel)-igrid+1)

     ! Loop over cells
     do ind=1,twotondim

        ! Compute cell centre position in code units
        do idim=1,ndim
           nstride=2**(idim-1)
           do i=1,ngrid
              xx(i,idim)=(2*m%grid(igrid+i-1)%ckey(idim)+MOD((ind-1)/nstride,2)+0.5)*dx
           end do
        end do

        ! Call analytical gravity routine
        call grav_ana(xx,ff,dx,ngrid,r%gravity_type,r%gravity_params)

        ! Scatter variables to main memory
        do idim=1,ndim
           do i=1,ngrid
              m%grid(igrid+i-1)%f(ind,idim)=ff(i,idim)
           end do
        end do

     end do
     ! End loop over cells

  end do
  ! End loop over grid

#endif
  
end subroutine force_analytic
!#########################################################
!#########################################################
!#########################################################
!#########################################################
recursive subroutine r_gradient_phi(pst,input_size,output_size,input_array)
  use ramses_commons, only: pst_t
  use mdl_parameters
  implicit none
  type(pst_t)::pst
  integer::input_size,output_size
  integer,dimension(1:input_size)::input_array

  integer::ilevel,icount

  if(pst%nLower>0)then
     call mdl_send_request(pst%s%mdl,MDL_GRADIENT_PHI,pst%iUpper+1,input_size,output_size,input_array)
     call r_gradient_phi(pst%pLower,input_size,output_size,input_array)
     call mdl_get_reply(pst%s%mdl,pst%iUpper+1,output_size)
  else
     ilevel=input_array(1)
     icount=input_array(2)
     call gradient_phi(pst%s,ilevel,icount)
  endif

end subroutine r_gradient_phi
!#########################################################
!#########################################################
!#########################################################
!#########################################################
subroutine gradient_phi(s,ilevel,icount)
  use amr_parameters, only: ndim,twondim,twotondim,threetondim,nvector,dp
  use ramses_commons, only: ramses_t
  use cache_commons
  implicit none
  type(ramses_t)::s
  integer::ilevel,icount
  !-------------------------------------------------
  ! This routine compute the 3-force for all cells
  ! in the current level grids, using a
  ! 5 nodes kernel (5 points FDA).
  !-------------------------------------------------
  integer,external::get_grid
  integer::i_nbor,igrid,idim,ind,igridn
  integer::id1,id2,id3,id4
  integer::ig1,ig2,ig3,ig4
  integer,dimension(1:3,1:4,1:8)::ggg,hhh
  integer,dimension(1:8,1:8)::ccc
  integer,dimension(1:threetondim),save::igrid_nbor,ind_nbor
  integer,dimension(1:3,1:6),save::shift=reshape(&
       & (/-1,0,0,1,0,0,0,-1,0,0,1,0,0,0,-1,0,0,1/),(/3,6/))
  integer(kind=8),dimension(0:ndim)::hash_nbor
  real(dp),dimension(1:8)::bbb
  real(dp)::dx,a,b,aa,bb,cc,dd,tfrac
  real(dp)::phi1,phi2,phi3,phi4
  real(dp),dimension(1:twotondim,0:twondim),save::phi_nbor

#ifdef GRAV

  associate(r=>s%r,g=>s%g,m=>s%m)

  ! Mesh size at level ilevel in code units
  dx=r%boxlen/2**ilevel

  ! Rescaling factor
  a=0.50D0*4.0D0/3.0D0/dx
  b=0.25D0*1.0D0/3.0D0/dx
  !   |dim
  !   | |node
  !   | | |cell
  !   v v v
  ggg(1,1,1:8)=(/1,0,1,0,1,0,1,0/); hhh(1,1,1:8)=(/2,1,4,3,6,5,8,7/)
  ggg(1,2,1:8)=(/0,2,0,2,0,2,0,2/); hhh(1,2,1:8)=(/2,1,4,3,6,5,8,7/)
  ggg(1,3,1:8)=(/1,1,1,1,1,1,1,1/); hhh(1,3,1:8)=(/1,2,3,4,5,6,7,8/)
  ggg(1,4,1:8)=(/2,2,2,2,2,2,2,2/); hhh(1,4,1:8)=(/1,2,3,4,5,6,7,8/)
  ggg(2,1,1:8)=(/3,3,0,0,3,3,0,0/); hhh(2,1,1:8)=(/3,4,1,2,7,8,5,6/)
  ggg(2,2,1:8)=(/0,0,4,4,0,0,4,4/); hhh(2,2,1:8)=(/3,4,1,2,7,8,5,6/)
  ggg(2,3,1:8)=(/3,3,3,3,3,3,3,3/); hhh(2,3,1:8)=(/1,2,3,4,5,6,7,8/)
  ggg(2,4,1:8)=(/4,4,4,4,4,4,4,4/); hhh(2,4,1:8)=(/1,2,3,4,5,6,7,8/)
  ggg(3,1,1:8)=(/5,5,5,5,0,0,0,0/); hhh(3,1,1:8)=(/5,6,7,8,1,2,3,4/)
  ggg(3,2,1:8)=(/0,0,0,0,6,6,6,6/); hhh(3,2,1:8)=(/5,6,7,8,1,2,3,4/)
  ggg(3,3,1:8)=(/5,5,5,5,5,5,5,5/); hhh(3,3,1:8)=(/1,2,3,4,5,6,7,8/)
  ggg(3,4,1:8)=(/6,6,6,6,6,6,6,6/); hhh(3,4,1:8)=(/1,2,3,4,5,6,7,8/)

  ! CIC method constants
  aa = 1.0D0/4.0D0**ndim
  bb = 3.0D0*aa
  cc = 9.0D0*aa
  dd = 27.D0*aa
  bbb(:)  =(/aa ,bb ,bb ,cc ,bb ,cc ,cc ,dd/)

  ! Sampling positions in the 3x3x3 father cell cube
  ccc(:,1)=(/1 ,2 ,4 ,5 ,10,11,13,14/)
  ccc(:,2)=(/3 ,2 ,6 ,5 ,12,11,15,14/)
  ccc(:,3)=(/7 ,8 ,4 ,5 ,16,17,13,14/)
  ccc(:,4)=(/9 ,8 ,6 ,5 ,18,17,15,14/)
  ccc(:,5)=(/19,20,22,23,10,11,13,14/)
  ccc(:,6)=(/21,20,24,23,12,11,15,14/)
  ccc(:,7)=(/25,26,22,23,16,17,13,14/)
  ccc(:,8)=(/27,26,24,23,18,17,15,14/)

  if (icount .ne. 1 .and. icount .ne. 2)then
     write(*,*)'icount has bad value'
     call mdl_abort
  endif

  ! Compute fraction of time steps for interpolation
  if (g%dtold(ilevel-1)>0.0d0)then
     tfrac=g%dtnew(ilevel)/g%dtold(ilevel-1)*(icount-1)
  else
     tfrac=0.0
  end if

  call open_cache(s,operation_interpol,domain_decompos_amr)

  hash_nbor(0)=ilevel

  ! Loop over grids
  do igrid=m%head(ilevel),m%tail(ilevel)
     
     ! Get central oct potential
     do ind=1,twotondim
        phi_nbor(ind,0)=m%grid(igrid)%phi(ind)
     end do

     ! Get neighboring octs potential
     do i_nbor=1,twondim

        ! Get neighboring grid
        hash_nbor(1:ndim)=m%grid(igrid)%ckey(1:ndim)+shift(1:ndim,i_nbor)
        ! Periodic boundary conditons
        do idim=1,ndim
           if(hash_nbor(idim)<0)hash_nbor(idim)=m%ckey_max(ilevel)-1
           if(hash_nbor(idim)==m%ckey_max(ilevel))hash_nbor(idim)=0
        enddo
        igridn=get_grid(s,hash_nbor,m%grid_dict,.false.,.true.)

        ! If grid exists, then copy into array
        if(igridn>0)then
           do ind=1,twotondim
              phi_nbor(ind,i_nbor)=m%grid(igridn)%phi(ind)
           end do

        ! Otherwise interpolate from coarser level
        else
           ! Get 3**ndim parent cell using read-only cache
           call get_threetondim_nbor_parent_cell(s,hash_nbor,m%grid_dict,igrid_nbor,ind_nbor,.false.,.true.)
           call interpol_phi(m,igrid_nbor,ind_nbor,ccc,bbb,tfrac,phi_nbor(1,i_nbor))
           do ind=1,threetondim
              call unlock_cache(s,igrid_nbor(ind))
           end do
        endif

     end do
     ! End loop over neighboring octs

     ! Loop over cells
     do ind=1,twotondim

        ! Loop over dimensions
        do idim=1,ndim

           ! Gather nodes indices
           id1=hhh(idim,1,ind); ig1=ggg(idim,1,ind)
           id2=hhh(idim,2,ind); ig2=ggg(idim,2,ind)
           id3=hhh(idim,3,ind); ig3=ggg(idim,3,ind)
           id4=hhh(idim,4,ind); ig4=ggg(idim,4,ind)

           ! Gather potential
           phi1=phi_nbor(id1,ig1)
           phi2=phi_nbor(id2,ig2)
           phi3=phi_nbor(id3,ig3)
           phi4=phi_nbor(id4,ig4)

           ! Compute acceleration
           m%grid(igrid)%f(ind,idim)=a*(phi1-phi2)-b*(phi3-phi4)

        end do
        ! End loop over dimensions

     end do
     ! End loop over cells

  end do
  ! End loop over grids

  call close_cache(s,m%grid_dict)

  end associate
  
#endif

end subroutine gradient_phi
!#########################################################
!#########################################################
!#########################################################
!#########################################################
recursive subroutine r_compute_epot(pst,input_size,output_size,ilevel,output_array)
  use ramses_commons, only: pst_t
  use mdl_parameters
  implicit none
  type(pst_t)::pst
  integer::input_size,output_size
  integer::ilevel
  integer,dimension(1:output_size)::output_array

  integer,dimension(1:output_size)::next_output_array
  real(kind=8)::epot,next_epot

  if(pst%nLower>0)then
     call mdl_send_request(pst%s%mdl,MDL_COMPUTE_EPOT,pst%iUpper+1,input_size,output_size,ilevel)
     call r_compute_epot(pst%pLower,input_size,output_size,ilevel,output_array)
     call mdl_get_reply(pst%s%mdl,pst%iUpper+1,output_size,next_output_array)
     epot=transfer(output_array,epot)
     next_epot=transfer(next_output_array,next_epot)
     epot=epot+next_epot
     output_array=transfer(epot,output_array)
  else
     call compute_epot(pst%s%r,pst%s%g,pst%s%m,ilevel,epot)
     output_array=transfer(epot,output_array)
  endif

end subroutine r_compute_epot
!#########################################################
!#########################################################
!#########################################################
!#########################################################
subroutine compute_epot(r,g,m,ilevel,epot)
  use amr_parameters, only: ndim,twotondim,dp
  use amr_commons, only: run_t,global_t,mesh_t
  implicit none
  type(run_t)::r
  type(global_t)::g
  type(mesh_t)::m
  integer::ilevel
  real(kind=8)::epot
  !----------------------------------------------------------
  ! This routine computes the potential energy
  !----------------------------------------------------------
  integer::igrid,ind,idim
  real(dp)::dx,fact,fourpi
 
#ifdef GRAV  

  ! Mesh size at level ilevel in code units
  dx=r%boxlen/2**ilevel
  ! Local constants
  fourpi=4.0D0*ACOS(-1.0D0)
  if(r%cosmo)fourpi=1.5D0*g%omega_m*g%aexp
  fact=-dx**ndim/fourpi/2.0D0

  ! Compute gravity potential
  epot=0D0

  ! Loop over myid grids by vector sweeps
  do igrid=m%head(ilevel),m%tail(ilevel)
     ! Loop over cells
     do ind=1,twotondim
        ! Loop over dimensions
        do idim=1,ndim
           if(.not.m%grid(igrid)%refined(ind))then
              epot=epot+fact*m%grid(igrid)%f(ind,idim)**2
           endif
        end do
     end do
     ! End loop over cells
  end do
  ! End loop over grids

#endif  

end subroutine compute_epot
!#########################################################
!#########################################################
!#########################################################
!#########################################################
recursive subroutine r_compute_rhomax(pst,input_size,output_size,ilevel,output_array)
  use ramses_commons, only: pst_t
  use mdl_parameters
  implicit none
  type(pst_t)::pst
  integer::input_size,output_size
  integer::ilevel
  integer,dimension(1:output_size)::output_array

  integer,dimension(1:output_size)::next_output_array
  real(kind=8)::rhomax,next_rhomax

  if(pst%nLower>0)then
     call mdl_send_request(pst%s%mdl,MDL_COMPUTE_RHOMAX,pst%iUpper+1,input_size,output_size,ilevel)
     call r_compute_rhomax(pst%pLower,input_size,output_size,ilevel,output_array)
     call mdl_get_reply(pst%s%mdl,pst%iUpper+1,output_size,next_output_array)
     rhomax=transfer(output_array,rhomax)
     next_rhomax=transfer(next_output_array,next_rhomax)
     rhomax=MAX(rhomax,next_rhomax)
     output_array=transfer(rhomax,output_array)
  else
     call compute_rhomax(pst%s%r,pst%s%g,pst%s%m,ilevel,rhomax)
     output_array=transfer(rhomax,output_array)
  endif

end subroutine r_compute_rhomax
!#########################################################
!#########################################################
!#########################################################
!#########################################################
subroutine compute_rhomax(r,g,m,ilevel,rhomax)
  use amr_parameters, only: ndim,twotondim,dp
  use amr_commons, only: run_t,global_t,mesh_t
  implicit none
  type(run_t)::r
  type(global_t)::g
  type(mesh_t)::m
  integer::ilevel
  real(kind=8)::rhomax
  !----------------------------------------------------------
  ! This routine computes the potential energy
  !----------------------------------------------------------
  integer::igrid,ind,idim
 
#ifdef GRAV  

  ! Compute maximum total mass density
  rhomax=0D0

  ! Loop over myid grids by vector sweeps
  do igrid=m%head(ilevel),m%tail(ilevel)
     ! Loop over cells
     do ind=1,twotondim
        rhomax=MAX(rhomax,dble(abs(m%grid(igrid)%rho(ind))))
     end do
     ! End loop over cells
  end do
  ! End loop over grids

#endif  

end subroutine compute_rhomax
!#########################################################
!#########################################################
!#########################################################
!#########################################################






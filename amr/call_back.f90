module call_back
  
  interface
     subroutine ramses_function(pst,input_size,output_size,input,output)
       use ramses_commons, only: pst_t
       type(pst_t)::pst
       integer::input_size,output_size
       integer,dimension(1:input_size),optional::input
       integer,dimension(1:output_size),optional::output
     end subroutine ramses_function
  end interface
  
  procedure(ramses_function)::r_clean_stop,r_set_add,r_broadcast_params,r_broadcast_global
  procedure(ramses_function)::r_init_amr,r_init_time,r_init_hydro,r_init_part
  procedure(ramses_function)::r_input_part_grafic,r_input_part_ascii,r_input_part_restart
  procedure(ramses_function)::r_init_flag,r_user_flag,r_ensure_ref_rules
  procedure(ramses_function)::r_collect_noct,r_noct_tot,r_noct_min,r_noct_max
  procedure(ramses_function)::r_noct_used_max,r_gather_noct_max,r_npart_max
  procedure(ramses_function)::r_init_refine_basegrid,r_init_refine_restart
  procedure(ramses_function)::r_broadcast_bound_key,r_collect_bound_key
  procedure(ramses_function)::r_load_balance,r_balance_part
  procedure(ramses_function)::r_refine_fine,r_smooth_fine
  procedure(ramses_function)::r_input_hydro_condinit,r_input_hydro_grafic
  procedure(ramses_function)::r_upload_fine
  procedure(ramses_function)::r_multipole_leaf_cells,r_multipole_split_cells
  procedure(ramses_function)::r_reset_rho,r_cic_multipole,r_cic_part,r_split_part
  procedure(ramses_function)::r_kick_drift_part
  procedure(ramses_function)::r_mass_min_part,r_broadcast_mp_min
  procedure(ramses_function)::r_collect_multipole,r_broadcast_multipole
  procedure(ramses_function)::r_output_amr,r_output_hydro,r_output_poisson,r_output_part
  procedure(ramses_function)::r_synchro_hydro_fine,r_force_analytic,r_gradient_phi
  procedure(ramses_function)::r_save_phi_old,r_compute_epot,r_compute_rhomax
  procedure(ramses_function)::r_broadcast_aexp,r_courant_fine,r_godunov_fine
  procedure(ramses_function)::r_set_unew,r_set_uold,r_gravity_hydro_fine
  procedure(ramses_function)::r_cooling_fine,r_newdt_part,r_broadcast_dt
  procedure(ramses_function)::r_make_initial_phi,r_init_mg,r_build_mg,r_cleanup_mg
  procedure(ramses_function)::r_recurrence_on_p,r_recurrence_x_and_r
  procedure(ramses_function)::r_cmp_residual_cg,r_cmp_Ap_cg
  procedure(ramses_function)::r_cmp_rhs_norm,r_cmp_r2_cg,r_cmp_pAp_cg
  procedure(ramses_function)::r_make_mask,r_make_bc_rhs,r_restrict_mask
  procedure(ramses_function)::r_cmp_residual_mg,r_gauss_seidel_mg,r_reset_correction
  procedure(ramses_function)::r_restrict_residual,r_interpolate_and_correct
  procedure(ramses_function)::r_set_scan_flag,r_cmp_residual_norm2
  procedure(ramses_function)::r_output_frame
  
  type call_back_f
     procedure(ramses_function),pointer,nopass::proc
  end type call_back_f

  interface
     subroutine cache_function(grid,msg_size,msg_array)
       use amr_commons, only: oct
       type(oct)::grid
       integer::msg_size
       integer,dimension(1:msg_size),optional::msg_array
     end subroutine cache_function
  end interface

  procedure(cache_function)::pack_fetch_flag,pack_fetch_refine,pack_fetch_split,pack_fetch_hydro,pack_fetch_cg,pack_fetch_phi
  procedure(cache_function)::pack_fetch_restrict_res,pack_fetch_scan,pack_fetch_mg,pack_fetch_interpol,pack_fetch_kick
  procedure(cache_function)::unpack_fetch_flag,unpack_fetch_refine,unpack_fetch_split,unpack_fetch_hydro,unpack_fetch_cg,unpack_fetch_phi
  procedure(cache_function)::unpack_fetch_restrict_res,unpack_fetch_scan,unpack_fetch_mg,unpack_fetch_interpol,unpack_fetch_kick
  procedure(cache_function)::init_flush_initflag,init_flush_derefine,init_flush_upload,init_flush_multipole,init_flush_rho
  procedure(cache_function)::init_flush_restrict_mask,init_flush_restrict_res,init_flush_godunov
  procedure(cache_function)::pack_flush_initflag,pack_flush_derefine,pack_flush_upload,pack_flush_multipole,pack_flush_rho
  procedure(cache_function)::pack_flush_build_mg,pack_flush_restrict_mask,pack_flush_restrict_res,pack_flush_godunov,pack_flush_refine
  procedure(cache_function)::pack_flush_loadbalance
  procedure(cache_function)::unpack_flush_initflag,unpack_flush_derefine,unpack_flush_upload,unpack_flush_multipole,unpack_flush_rho
  procedure(cache_function)::unpack_flush_build_mg,unpack_flush_restrict_mask,unpack_flush_restrict_res,unpack_flush_godunov,unpack_flush_refine
  procedure(cache_function)::unpack_flush_loadbalance

  type cache_f
     procedure(cache_function),pointer,nopass::proc
  end type cache_f
     
end module call_back

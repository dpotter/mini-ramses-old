program ramses

#ifndef MDL2
  use mdl_commons
  call mdl_init
#else
  use pst

  type :: service2_in_t
    real::x
    real::y
    real::z
    integer::n
  end type service2_in_t

  call mdl_launch( command_argument_count(), C_NULL_PTR, C_FUNLOC(master), C_FUNLOC(worker) )

contains 

subroutine worker(mdl)
  implicit none
  type(mdl2_t)::mdl
  type(pst_t) :: pst
  type(ramses_t)::s

  write(*,*) 'worker function invoked, id=',mdl%myid,"rank=",mdl%irank,"thread=",mdl%icore
  call pst_initialize(pst,mdl,s)
  call mdl_handler(mdl)
end subroutine worker

subroutine master(mdl)
  type(mdl2_t)::mdl
  integer::i,rid,n
  real::in1
  type(service2_in_t)::in2
  real::v
  type(pst_t)::pst
  type(ramses_t)::s

  write(*,*) 'master function invoked, id=',mdl_self(mdl),"of",mdl_threads(mdl)
  call pst_initialize(pst,mdl,s)
  call r_pst_set_add(pst,mdl_threads(mdl),8,n,n)

  write(*,*) 'invoking service 1'
  in1 = 10.0
  call r_service1(pst,in1,storage_size(in1)/8,n,n)

  write(*,*) 'invoking service 2'
  call r_service2(pst,in2,storage_size(in2)/8,n,n)

  do i = 1,mdl%ncpu-1
    rid = mdl_req_service(mdl,i,MDL_CLEAN_STOP,0,0)
    call mdl_get_reply(mdl,rid,0,n)
  end do

end subroutine master

subroutine pst_initialize(pst,mdl,s)
  implicit none
  type(pst_t)::pst
  type(mdl2_t),target::mdl 
  type(ramses_t),target::s
  pst%mdl => mdl
  pst%s => s

  call mdl_add_service(mdl,MDL_CLEAN_STOP,mdl,C_FUNLOC(service0),0,0)
  call mdl_add_service(mdl,MDL_SET_ADD,pst,C_FUNLOC(r_pst_set_add),8,0)
  call mdl_add_service(mdl,PST_SERVICE1,pst,C_FUNLOC(r_service1),8,0)
  call mdl_add_service(mdl,PST_SERVICE2,pst,C_FUNLOC(r_service2),16,0)
  call mdl_commit_services(mdl)
end subroutine pst_initialize

! Service 0 is called when we shutdown
subroutine service0(mdl,In,nIn,Out,nOut)
  implicit none
  type(mdl2_t)::mdl
  integer(c_int),VALUE::nIn ! Size of the input structure
  integer(c_int)::nOut    ! Size of the result (set)
  real::In    ! Input type/structure
  integer(c_int)::Out   ! Output type/structure

  write(*,*) 'Service 0 (stop) invoked on thread',mdl%myid,"input=",In

  nOut=0
end subroutine service0

recursive subroutine r_service1(pst,In,nIn,Out,nOut)
  implicit none
  type(pst_t)::pst
  type(mdl2_t),pointer::mdl
  integer(c_int),VALUE::nIn ! Size of the input structure
  integer(c_int)::nOut    ! Size of the result (set)

  real::In    ! Input type/structure
  integer(c_int)::Out   ! Output type/structure

  integer::rID

  mdl => pst%mdl
  if (pst%nLower > 0) then
    rID = mdl_req_service(pst%mdl,pst%iUpper,PST_SERVICE1,In + pst%nLower,nIn)
    call r_service1(pst%pLower,In,nIn,Out,nOut)
    call mdl_get_reply(pst%mdl,rID,c_null_ptr,nOut)
  else
    write(*,*) 'Service 1 invoked on thread',pst%mdl%myid,'input=',In
  end if
  nOut=0
end subroutine r_service1

recursive subroutine r_service2(pst,In,nIn,Out,nOut)
  implicit none
  type(pst_t)::pst
  integer(c_int),VALUE::nIn	! Size of the input structure
  integer(c_int)::nOut		! Size of the result (set)

  type(service2_in_t)::In		! Input type/structure
  integer(c_int)::Out		! Output type/structure
  integer::rID

  if (pst%nLower > 0) then
    rID = mdl_req_service(pst%mdl,pst%iUpper,PST_SERVICE2,In,nIn)
    call r_service2(pst%pLower,In,nIn,Out,nOut)
    call mdl_get_reply(pst%mdl,rID,c_null_ptr,nOut)
  else
    write(*,*) 'Service 2 invoked on thread',pst%mdl%myid
  end if
  nOut=0
end subroutine r_service2
#endif

end program ramses


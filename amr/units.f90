subroutine units(r,g,scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2)
  use amr_parameters, only: dp,ndim,kB,mH,X_H,rhoc
  use amr_commons, only: run_t,global_t,mesh_t
  implicit none
  type(run_t)::r
  type(global_t)::g
  real(dp)::scale_nH,scale_T2,scale_t,scale_v,scale_d,scale_l
  !-----------------------------------------------------------------------
  ! Conversion factors from user units into cgs units
  ! For gravity runs, make sure that G=1 in user units.
  !-----------------------------------------------------------------------

  ! scale_d converts mass density from user units into g/cc
  scale_d = r%units_density
  if(r%cosmo) scale_d = g%omega_m * rhoc *(g%h0/100.)**2 / g%aexp**3

  ! scale_t converts time from user units into seconds
  scale_t = r%units_time
  if(r%cosmo) scale_t = g%aexp**2 / (g%h0*1d5/3.08d24)

  ! scale_l converts distance from user units into cm
  scale_l = r%units_length
  if(r%cosmo) scale_l = g%aexp * g%boxlen_ini * 3.08d24 / (g%h0/100)

  ! scale_v converts velocity in user units into cm/s
  scale_v = scale_l / scale_t

  ! scale_T2 converts (P/rho) in user unit into (T/mu) in Kelvin
  scale_T2 = mH/kB * scale_v**2

  ! scale_nH converts rho in user units into nH in H/cc
  scale_nH = X_H/mH * scale_d

end subroutine units

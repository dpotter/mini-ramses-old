module pst
  USE, INTRINSIC :: ISO_C_BINDING
  use mdl
  use mdl_parameters
  use ramses_commons, only: ramses_t
  type :: pst_t
    type(pst_t),pointer::pLower => null()
    type(mdl2_t),pointer::mdl => null()
    type(ramses_t),pointer::s => null()
    integer::iUpper = -1
    integer::nLower = 0
    integer::nUpper = 0
  end type pst_t
  enum,bind(C)
    enumerator::PST_SETADD
    enumerator::PST_SERVICE1
    enumerator::PST_SERVICE2
  end enum
contains
! Contruct the Processor Service Tree
recursive subroutine r_pst_set_add(pst,iUpper,nIn,Out,nOut)
  implicit none
  type(pst_t)::pst
  integer(c_int),VALUE::nIn ! Size of the input structure
  integer(c_int)::nOut      ! Size of the result (set)
  integer::iUpper           ! ID of upper
  integer(c_int)::Out       ! unused

  integer::n,iLower,iMiddle,iProcLower,iProcUpper
  integer::rID

  iLower = mdl_self(pst%mdl)
  n = iUpper - iLower
  iMiddle = (iUpper + iLower) / 2
  if (n>1) then
    ! Make sure that we land on core zero of the remote node
    iProcLower = mdl_thread_to_proc(pst%mdl,iLower)
    iProcUpper = mdl_thread_to_proc(pst%mdl,iUpper-1)
    if (iProcLower /= iProcUpper) then
      iMiddle = mdl_proc_to_thread(pst%mdl,mdl_thread_to_proc(pst%mdl,iMiddle))
    end if
    pst%iUpper = iMiddle;
    pst%nLower = iMiddle - iLower;
    pst%nUpper = iUpper - iMiddle;
    rID = mdl_req_service(pst%mdl,pst%iUpper,MDL_SET_ADD,iUpper,storage_size(iUpper)/8);
    allocate(pst%pLower)
    pst%pLower%mdl => pst%mdl
    pst%pLower%s => pst%s
    call r_pst_set_add(pst%pLower,iMiddle,nIn,Out,nOut)
    call mdl_get_reply(pst%mdl,rID,c_null_ptr,nOut)
  end if
end subroutine r_pst_set_add
end module pst
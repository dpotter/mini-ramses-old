!#########################################################################
!#########################################################################
!#########################################################################
!#########################################################################
subroutine m_dump_all(pst)
  use amr_parameters, only: ndim,flen
  use ramses_commons, only: pst_t
  implicit none
  type(pst_t)::pst

  ! Local variables
  integer::i
#ifdef NOSYSTEM
  integer::ierr
#endif
  character(LEN=5)::nchar
  character(LEN=flen)::filename,filedir,filecmd
  integer,dimension(1:flen/4)::input_array

  associate(r=>pst%s%r,g=>pst%s%g,m=>pst%s%m,p=>pst%s%p,mdl=>pst%s%mdl)

  if(g%nstep_coarse==g%nstep_coarse_old.and.g%nstep_coarse>0)return
  if(g%nstep_coarse==0.and.r%nrestart>0)return
  if(r%verbose)write(*,*)'Entering dump_all'

  ! For 1D runs, output data to screen
  do i=r%levelmin,r%nlevelmax
     call write_screen(m,i)
  end do

  ! Increment output counters
  call title(g%ifout,nchar)
  g%ifout=g%ifout+1
  if(g%t>=r%tout(g%iout).or.g%aexp>=r%aout(g%iout))g%iout=g%iout+1
  g%output_done=.true.

  ! For 2D and 3D runs, output data to files
  if(ndim>1)then
     filedir='output_'//TRIM(nchar)//'/'
     filecmd='mkdir -p '//TRIM(filedir)
#ifdef NOSYSTEM
     call PXFMKDIR(TRIM(filedir),LEN(TRIM(filedir)),O'755',ierr)
#else
     call system(filecmd)
#endif

     !-----------------------
     ! Only master process
     !-----------------------

     if(r%pic)then
        filename=TRIM(filedir)//'part_header.txt'
        call output_header(r,g,p,filename)
     endif
     if(r%hydro)then
        filename=TRIM(filedir)//'hydro_file_descriptor.txt'
        call file_descriptor_hydro(r,filename)
     end if
     filename=TRIM(filedir)//'info.txt'
     call output_info(r,g,filename)
     filename=TRIM(filedir)//'makefile.txt'
     call output_makefile(filename)
     filename=TRIM(filedir)//'patches.txt'
     call output_patch(filename)
     filename=TRIM(filedir)//'namelist.txt'
     call output_namelist(filename)
     filename=TRIM(filedir)//'compilation.txt'
     call output_compil(filename)
     filename=TRIM(filedir)//'params.out'
     call output_params(r,g,m,filename)

     !-----------------------
     ! All slave processes
     !-----------------------

     ! Output AMR data
     filename=TRIM(filedir)//'amr.out'
     input_array=transfer(filename,input_array)
     call r_output_amr(pst,flen/4,0,input_array)
     
     ! Output HYDRO data
     if(r%hydro)then
        filename=TRIM(filedir)//'hydro.out'
        input_array=transfer(filename,input_array)
        call r_output_hydro(pst,flen/4,0,input_array)
     end if

     ! Output GRAV data
     if(r%poisson)then
        filename=TRIM(filedir)//'grav.out'
        input_array=transfer(filename,input_array)
        call r_output_poisson(pst,flen/4,0,input_array)
     end if

     ! Output PART data
     if(r%pic)then
        filename=TRIM(filedir)//'part.out'
        input_array=transfer(filename,input_array)
        call r_output_part(pst,flen/4,0,input_array)
     end if
  end if

  end associate
  
end subroutine m_dump_all
!#########################################################################
!#########################################################################
!#########################################################################
!#########################################################################
subroutine output_namelist(filename)
  use amr_parameters, only: flen
  use amr_commons
  use pm_commons
  use hydro_commons
  implicit none
  character(LEN=flen)::filename
  ! Copy namelist file to output directory
  character::nml_char
  integer::ierr

  open(10,file=namelist_file,access="stream",action="read")
  open(11,file=filename,access="stream",action="write")
  do 
     read(10,iostat=ierr)nml_char 
     if(ierr.NE.0)exit
     write(11)nml_char 
  end do
  close(10)
  close(11)

end subroutine output_namelist
!#########################################################################
!#########################################################################
!#########################################################################
!#########################################################################
subroutine output_compil(filename)
  use amr_parameters, only: flen
  use amr_commons
  use pm_commons
  use hydro_commons
  implicit none
  character(LEN=flen)::filename
  ! Copy compilation details to output directory
  OPEN(UNIT=11, FILE=filename, FORM='formatted')
  write(11,'(" compile date = ",A)')TRIM(builddate)
  write(11,'(" patch dir    = ",A)')TRIM(patchdir)
  write(11,'(" remote repo  = ",A)')TRIM(gitrepo)
  write(11,'(" local branch = ",A)')TRIM(gitbranch)
  write(11,'(" last commit  = ",A)')TRIM(githash)
  CLOSE(11)
end subroutine output_compil
!#########################################################################
!#########################################################################
!#########################################################################
!#########################################################################
subroutine output_params(r,g,m,filename)
  use amr_parameters, only: ndim,nhilbert,flen
  use amr_commons, only: run_t,global_t,mesh_t
  implicit none
  type(run_t)::r
  type(global_t)::g
  type(mesh_t)::m
  character(LEN=flen)::filename

  ! Local variables
  integer::ilun,ilevel
  character(LEN=flen)::fileloc

  if(r%verbose)write(*,*)'Entering output_params'

  !-----------------------------------
  ! Output run parameters in file
  !-----------------------------------  
  ilun=10
  fileloc=TRIM(filename)
  open(unit=ilun,file=fileloc,access="stream"&
       & ,action="write",form='unformatted')
  ! Write grid variables
  write(ilun)g%ncpu
  write(ilun)ndim
  write(ilun)r%levelmin
  write(ilun)r%nlevelmax
  write(ilun)r%boxlen
  ! Write time variables
  write(ilun)r%noutput,g%iout,g%ifout
  write(ilun)r%tout(1:r%noutput)
  write(ilun)r%aout(1:r%noutput)
  write(ilun)g%t
  write(ilun)g%dtold(1:r%nlevelmax)
  write(ilun)g%dtnew(1:r%nlevelmax)
  write(ilun)g%nstep,g%nstep_coarse
  ! Write various constants
  write(ilun)g%const,g%mass_tot_0,g%rho_tot
  write(ilun)g%omega_m,g%omega_l,g%omega_k,g%omega_b,g%h0,g%aexp_ini,g%boxlen_ini
  write(ilun)g%aexp,g%hexp,g%aexp_old,g%epot_tot_int,g%epot_tot_old
  write(ilun)r%mass_sph
  ! Write cpu boundaries
  write(ilun)nhilbert
  do ilevel=r%levelmin,r%nlevelmax
     write(ilun)m%domain(ilevel)%b(1:nhilbert,0:g%ncpu)
  end do
  close(ilun)

end subroutine output_params
!#########################################################################
!#########################################################################
!#########################################################################
!#########################################################################
subroutine input_params(r,g,filename,ncpu_file,levelmin_file,nlevelmax_file)
  use amr_parameters, only: ndim,nhilbert,dp,flen
  use amr_commons, only: run_t,global_t
  implicit none
  type(run_t)::r
  type(global_t)::g
  character(LEN=flen)::filename
  integer::ncpu_file,levelmin_file,nlevelmax_file,i
  !-----------------------------------
  ! Read run parameters from file.
  ! Note that ncpu, levelmin and nlevelmax
  ! are allowed to vary at restart.
  !-----------------------------------  
  integer::ilun
  integer::ndim_file,noutput_file
  integer::noutput_min,nlevelmax_min
  real(dp)::mass_sph_file
  character(LEN=flen)::fileloc

  if(r%verbose)write(*,*)'Entering input_params'

  ilun=10+g%myid
  fileloc=TRIM(filename)
  open(unit=ilun,file=fileloc,access="stream"&
       & ,action="read",form='unformatted')
  ! Read grid variables
  read(ilun)ncpu_file
  read(ilun)ndim_file
  read(ilun)levelmin_file
  read(ilun)nlevelmax_file
  ! Overwrite boxlen with value from file
  read(ilun)r%boxlen
  ! Read time variables
  read(ilun)noutput_file,g%iout,g%ifout
  noutput_min=MIN(r%noutput,noutput_file)
  read(ilun)r%tout(1:noutput_min)
  read(ilun)r%aout(1:noutput_min)
  read(ilun)g%t
  nlevelmax_min=MIN(r%nlevelmax,nlevelmax_file)
  read(ilun)g%dtold(1:nlevelmax_min)
  read(ilun)g%dtnew(1:nlevelmax_min)
  read(ilun)g%nstep,g%nstep_coarse
  ! Read various constants
  read(ilun)g%const,g%mass_tot_0,g%rho_tot
  read(ilun)g%omega_m,g%omega_l,g%omega_k,g%omega_b,g%h0,g%aexp_ini,g%boxlen_ini
  read(ilun)g%aexp,g%hexp,g%aexp_old,g%epot_tot_int,g%epot_tot_old
  read(ilun)mass_sph_file
  close(ilun)
  ! For cosmo runs only, as mass_sph is not set in the namelist
  if(r%cosmo)r%mass_sph=mass_sph_file
  ! Check dimensions
  if(ndim.NE.ndim_file)then
     if(g%myid==1)then
        write(*,*)'Incorrect number of space dimensions in restart file'
     endif
     call mdl_abort
  endif
  ! Compute movie frame number if applicable
  if(r%imovout>0) then
     do i=2,r%imovout
        if(r%aendmov>0)then
           if(g%aexp>r%amovout(i-1).and.g%aexp<r%amovout(i)) then
              r%imov=i
           endif
        else
           if(g%t>r%tmovout(i-1).and.g%t<r%tmovout(i)) then
              r%imov=i
           endif
        endif
     enddo
  endif

end subroutine input_params
!#########################################################################
!#########################################################################
!#########################################################################
!#########################################################################
recursive subroutine r_output_amr(pst,input_size,output_size,input_array)
  use amr_parameters, only: flen
  use ramses_commons, only: pst_t
  use mdl_parameters
  implicit none
  type(pst_t)::pst
  integer::input_size,output_size
  integer,dimension(1:input_size)::input_array
  
  character(LEN=flen)::filename
  
  if(pst%nLower>0)then
     call mdl_send_request(pst%s%mdl,MDL_OUTPUT_AMR,pst%iUpper+1,input_size,output_size,input_array)
     call r_output_amr(pst%pLower,input_size,output_size,input_array)
     call mdl_get_reply(pst%s%mdl,pst%iUpper+1,output_size)
  else
     filename=transfer(input_array,filename)
     call output_amr(pst%s%r,pst%s%g,pst%s%m,filename)
  endif

end subroutine r_output_amr
!#########################################################################
!#########################################################################
!#########################################################################
!#########################################################################
subroutine output_amr(r,g,m,filename)
  use amr_parameters, only: ndim,sp,dp,flen
  use amr_commons, only: run_t,global_t,mesh_t
  implicit none
  type(run_t)::r
  type(global_t)::g
  type(mesh_t)::m
  character(LEN=flen)::filename
  !-----------------------------------
  ! Output amr grid in file
  !-----------------------------------  
  integer::ilun,ilevel,igrid
  character(LEN=flen)::fileloc
  character(LEN=5)::nchar

  ilun=g%myid+10
  call title(g%myid,nchar)
  fileloc=TRIM(filename)//TRIM(nchar)
  open(unit=ilun,file=fileloc,access="stream",action="write",form='unformatted')
  write(ilun)ndim
  write(ilun)r%levelmin
  write(ilun)r%nlevelmax
  do ilevel=r%levelmin,r%nlevelmax
     write(ilun)m%noct(ilevel)
  end do
  do ilevel=r%levelmin,r%nlevelmax
     do igrid=m%head(ilevel),m%tail(ilevel)
        write(ilun)m%grid(igrid)%ckey
        write(ilun)m%grid(igrid)%refined
     end do
  end do
  close(ilun)  
end subroutine output_amr
!#########################################################################
!#########################################################################
!#########################################################################
!#########################################################################
subroutine output_info(r,g,filename)
  use amr_parameters, only: ndim,sp,dp,flen
  use amr_commons, only: run_t,global_t
  implicit none
  type(run_t)::r
  type(global_t)::g
  character(LEN=flen)::filename

  integer::ilun
  real(dp)::scale_nH,scale_T2,scale_l,scale_d,scale_t,scale_v
  character(LEN=flen)::fileloc

  if(r%verbose)write(*,*)'Entering output_info'

  ilun=11

  ! Conversion factor from user units to cgs units
  call units(r,g,scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2)

  ! Open file
  fileloc=TRIM(filename)
  open(unit=ilun,file=fileloc,form='formatted')
  
  ! Write run parameters
  write(ilun,'("ncpu        =",I11)')g%ncpu
  write(ilun,'("ndim        =",I11)')ndim
  write(ilun,'("levelmin    =",I11)')r%levelmin
  write(ilun,'("levelmax    =",I11)')r%nlevelmax
  write(ilun,'("ngridmax    =",I11)')r%ngridmax
  write(ilun,'("nstep_coarse=",I11)')g%nstep_coarse
  write(ilun,*)

  ! Write physical parameters
  write(ilun,'("boxlen      =",E23.15)')r%boxlen
  write(ilun,'("time        =",E23.15)')g%t
  write(ilun,'("aexp        =",E23.15)')g%aexp
  write(ilun,'("H0          =",E23.15)')g%h0
  write(ilun,'("omega_m     =",E23.15)')g%omega_m
  write(ilun,'("omega_l     =",E23.15)')g%omega_l
  write(ilun,'("omega_k     =",E23.15)')g%omega_k
  write(ilun,'("omega_b     =",E23.15)')g%omega_b
  write(ilun,'("unit_l      =",E23.15)')scale_l
  write(ilun,'("unit_d      =",E23.15)')scale_d
  write(ilun,'("unit_t      =",E23.15)')scale_t
  write(ilun,*)
  
  close(ilun)

end subroutine output_info
!#########################################################################
!#########################################################################
!#########################################################################
!#########################################################################
subroutine output_header(r,g,p,filename)
  use amr_parameters, only: flen
  use amr_commons, only: run_t,global_t
  use pm_commons, only: part_t
  implicit none
  type(run_t)::r
  type(global_t)::g
  type(part_t)::p
  character(LEN=flen)::filename

  ! Local variables
  integer::ilun
  character(LEN=flen)::fileloc

  if(r%verbose)write(*,*)'Entering output_header'
  
  ilun=g%myid+10
  
  ! Open file
  fileloc=TRIM(filename)
  open(unit=ilun,file=fileloc,form='formatted')
  
  ! Write header information
  write(ilun,*)'Total number of particles'
  write(ilun,*)p%npart_tot
  
  write(ilun,*)'Total number of files'
  write(ilun,*)g%ncpu
  
  ! Keep track of what particle fields are present
  write(ilun,*)'Particle fields'
  write(ilun,'(a)',advance='no')'pos vel mass iord level '
#ifdef OUTPUT_PARTICLE_POTENTIAL
  write(ilun,'(a)',advance='no')'phi '
#endif
  close(ilun)

end subroutine output_header
!#########################################################################
!#########################################################################
!#########################################################################
!#########################################################################
subroutine input_header(r,g,filename,npart_tot_file,ncpu_file)
  use amr_parameters, only: i8b,flen
  use amr_commons, only: run_t,global_t
  implicit none
  type(run_t)::r
  type(global_t)::g
  character(LEN=flen)::filename
  integer(i8b)::npart_tot_file
  integer::ncpu_file

  integer::ilun
  character(LEN=flen)::fileloc

  if(r%verbose)write(*,*)'Entering input_header'
  
  ilun=g%myid+10
  
  ! Write header information
  fileloc=TRIM(filename)
  open(unit=ilun,file=fileloc,form='formatted')  
  read(ilun,*)
  read(ilun,*)npart_tot_file
  read(ilun,*)
  read(ilun,*)ncpu_file
  close(ilun)

end subroutine input_header
!#########################################################################
!#########################################################################
!#########################################################################
!#########################################################################
#ifdef GADGET
subroutine savegadget(filename)
  use amr_parameters, only: flen
  use amr_commons
  use hydro_commons
  use pm_commons
  use gadgetreadfilemod
  implicit none
  character(LEN=flen)::filename
  TYPE (gadgetheadertype) :: header
  real,allocatable,dimension(:,:)::pos, vel
  integer(i8b),allocatable,dimension(:)::ids
  integer::i, idim, ipart
  real:: gadgetvfact
  integer::info
  integer(i8b)::npart_tot, npart_loc
  real, parameter:: RHOcrit = 2.7755d11

  npart_tot=npart

  allocate(pos(ndim, npart), vel(ndim, npart), ids(npart))
  gadgetvfact = 100.0 * boxlen_ini / aexp / SQRT(aexp)

  header%npart = 0
  header%npart(2) = npart
  header%mass = 0
  header%mass(2) = omega_m*RHOcrit*(boxlen_ini)**3/npart_tot/1.d10
  header%time = aexp
  header%redshift = 1.d0/aexp-1.d0
  header%flag_sfr = 0
  header%nparttotal = 0
#ifndef LONGINT
  header%nparttotal(2) = npart_tot
#else
  header%nparttotal(2) = MOD(npart_tot,4294967296)
#endif
  header%flag_cooling = 0
  header%numfiles = ncpu
  header%boxsize = boxlen_ini
  header%omega0 = omega_m
  header%omegalambda = omega_l
  header%hubbleparam = h0/100.0
  header%flag_stellarage = 0
  header%flag_metals = 0
  header%totalhighword = 0
#ifndef LONGINT
  header%totalhighword(2) = 0
#else
  header%totalhighword(2) = npart_tot/4294967296
#endif
  header%flag_entropy_instead_u = 0
  header%flag_doubleprecision = 0
  header%flag_ic_info = 0
  header%lpt_scalingfactor = 0
  header%unused = ' '

  do idim=1,ndim
     ipart=0
     do i=1,npartmax
        if(levelp(i)>0)then
           ipart=ipart+1
           if (ipart .gt. npart) then
                write(*,*) myid, "Ipart=",ipart, "exceeds", npart
                stop
           endif
           pos(idim, ipart)=xp(i,idim) * boxlen_ini
           vel(idim, ipart)=vp(i,idim) * gadgetvfact
           if (idim.eq.1) ids(ipart) = idp(i)
        end if
     end do
  end do

  call gadgetwritefile(filename, myid-1, header, pos, vel, ids)
  deallocate(pos, vel, ids)

end subroutine savegadget
#endif

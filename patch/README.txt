Mini-ramses adopts the RAMSES patching strategy. Code modifications which are very specific to a certain setup and thus not added to the main code should be placed here insiside a subdirectory. Adding the patch to this subdirectory in the Makefile will include the modified source files.

Useful RAMSES patches should be adapted and added here.
